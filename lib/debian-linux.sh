# shellcheck shell=dash

_newline="
"

version_split () {
	# Split into numbers and non-numeric strings, but break the non-
	# numeric strings at hyphens
	local version="$1"
	echo "$version" | grep -oP '(?:\d+|-?[^-\d]*)'
}

is_num () {
	local string="$1"
	case "$string" in
		*[!0-9]*) return 1 ;;
		* ) return 0 ;;
	esac
}

version_cmp () {
	local left_ver="$1"
	local right_ver="$2"

	local left_comp right_comp
	left_comp=$(version_split "$left_ver")
	right_comp=$(version_split "$right_ver")

	local padding
	padding=$(printf '\n#\n#\n')
	local left_compfile right_compfile
	left_compfile=$(mktemp)
	right_compfile=$(mktemp)

	echo "${left_comp}${padding}" > "$left_compfile"
	echo "${right_comp}${padding}" > "$right_compfile"
	paste "$left_compfile" "$right_compfile" | while read -r left right ; do
		local left_defined=true
		[ "$left" = "#" ] && left_defined=false
		local right_defined=true
		[ "$right" = "#" ] && right_defined=false

		# Do the components indicate pre-releases?
		local left_pre=false
		if $left_defined && { [ "$left" = "-rc" ] || [ "$left" = "-trunk" ] ; }  ; then
			left_pre=true
		fi
		local right_pre=false
		if $right_defined && { [ "$right" = "-rc" ] || [ "$right" = "-trunk" ] ; } ; then
			right_pre=true
		fi

		# Are the components numeric?
		local left_num=false
		if $left_defined && is_num "$left" ; then
			left_num=true
		fi
		local right_num=false
		if $right_defined && is_num "$right" ; then
			right_num=true
		fi

		# Pre-releases sort before anything, even end-of-string
		if $left_pre || $right_pre ; then
			! $right_pre && echo "-1" && return
			! $left_pre && echo "1" && return
		fi

		# End-of-string sorts before anything else.
		# End-of-string on both sides means equality.
		if ! $left_defined || ! $right_defined ; then
			$right_defined && echo "-1" && return
			$left_defined && echo "1" && return
			echo "0" && return
		fi

		# Use numeric comparison if both sides numeric.
		if $left_num && $right_num ; then
			if [ "$left" -lt "$right" ] ; then
				echo "-1" ; return
			fi
			if [ "$left" -gt "$right" ] ; then
				echo "1" ; return
			fi
		else
		# Otherwise use ASCII comparison.
		# Note that '.' > '-' thus 2.6.x.y > 2.6.x-z for any y, z.
			local orig sorted
			orig=$(printf "%s\n%s\n" "$left" "$right")
			sorted=$(echo "$orig" | sort)
			if [ "$left" = "$right" ] ; then
				continue
			fi
			if [ "$orig" = "$sorted" ] ; then
				echo "-1" && return
			else
				echo "1" && return
			fi
		fi
	done
	rm "$left_compfile"
	rm "$right_compfile"
}

files_in_boot () {
	if [ -n "${IMAGE_LIST_FILES_IN_BOOT:-}" ] ; then
		echo "$IMAGE_LIST_FILES_IN_BOOT" | tr ' ' '\n'
		return
	fi
	find /boot -maxdepth 1
}

image_list () {
	local images=""

	for img_path in $(files_in_boot | grep '^/boot/vmlinu[xz]-' | grep -v '.sig$') ; do
		img_ver="${img_path##/boot/vmlinu?-}"
		images=$(printf "%s\n%s\t%s\n" "$images" "$img_ver" "$img_path")
	done

	images=$(echo "$images" | tail +2)
	echo "$images"
}

file_exists () {
	local path="$1"
	if [ -n "${IMAGE_LIST_FILES_IN_BOOT:-}" ] ; then
		echo "$IMAGE_LIST_FILES_IN_BOOT" | grep -q "^$path\$" && return 0 || return 3
	fi
	test -f "$path"
}

readlink_f () {
	local path="$1"
	if [ -n "${READLINK_LINKS:-}" ] ; then
		local entry
		entry=$(echo "$READLINK_LINKS" | grep "^$path""[[:space:]]" || true)
		if [ -z "$entry" ] ; then
			echo "$path"
		else
			echo "$entry" | cut -f 2
		fi
		return
	fi
	readlink -f "$path"
}

sorted_images_initrd () {
	local sorted_images
	sorted_images=$($LINUX_VERSION list --paths | $LINUX_VERSION sort)
	echo "$sorted_images" | while read -r img_ver img_path ; do
		[ -z "$img_ver" ] && continue
		local img_initrd="/boot/initrd.img-${img_ver}"
		if ! file_exists "$img_initrd" ; then
			img_initrd=""
		fi
		printf "%s\t%s\t%s\n" "$img_ver" "$img_path" "$img_initrd"
	done
}


# Return the historical kernel image name stem for this architecture.
# This should only be used by linux-update-symlinks.  The actual image
# name stem used by kernel packages may change at any time.
image_stem () {
	local uname_m
	uname_m=$(uname -m)
	case $uname_m in
		mips|parisc|powerpc|ppc) echo "vmlinux" ;;
		*) echo "vmlinuz" ;;
	esac
}

trim () {
	local line="$1"
	line="${line# }"
	line="${line% }"
	echo "$line"
}

conf_assignments_from_file () {
	local conf_loc="$1"
	local line_num=1
	# shellcheck disable=SC2002
	cat "$conf_loc" | while IFS="$_newline" read -r line ; do
		# Delete line endings, comments and blank lines
		line="${line%%#*}"
		[ -z "$line" ] && continue
		line=$(echo "$line" | tr '[:space:]' ' ' | tr -s ' ')
		line=$(trim "$line")
		[ -z "$line" ] && continue

		# Historically this was done by matching against one
		# (path) or two (bool) regexps per parameter, with no
		# attempt to ensure that each line matched one.  We now
		# warn about syntax errors, but for backward compatibility
		# we never treat them as fatal.

		# Parse into name = value
#	    if (!/^\s*(\w+)\s*=\s*(.*)/) {
#		print STDERR "$conf_loc:$.: W: ignoring line with syntax error\n";
#		next;
#	    }
		# TODO: ignore in case of syntax error

		local name_raw value_raw
		name_raw=$(echo "$line" | cut -d= -f1)
		value_raw=$(echo "$line" | cut -d= -f2)
		local name value
		name=$(trim "$name_raw")
		value=$(trim "$value_raw")

		# Parse value according to expected type
		case $name in
			do_symlinks|link_in_boot|no_symlinks)
				case $value in
					no|false|0) value=false ;;
					yes|true|1) value=true ;;
					*) echo "${conf_loc}:${line_num}: W: ignoring invalid value \`\`$value''for $name" >&2 ;;
				esac
				echo "KERNELIMG_CONF_${name}=$value"
				;;
			image_dest)
				# Only one space-separated word is supported
				local path
				path=$(echo "$value" | cut -d ' ' -f 1)
				echo "KERNELIMG_CONF_${name}='$path'"
				;;
			do_bootloader|do_initrd)
				# These are still set in the jessie installer even though they
				# have no effect.  Ignore them quietly.
				;;
			clobber_modules|force_build_link|relink_build_link|relink_src_link|silent_modules|warn_reboot)
				# These are used only by kernel-package, and are not relevant to
				# anything that linux-base does.  Ignore them quietly.
				;;
			*)
				echo "${conf_loc}:${line_num}: W: ignoring unknown parameter $name" >&2
				;;
		esac
		line_num=$((line_num + 1))
	done
}

read_kernelimg_conf () {
	local conf_loc="/etc/kernel-img.conf"
	[ $# -eq 1 ] && conf_loc="$1"

	# Reset configuration variables.
	vars=$(set | grep ^KERNELIMG_CONF_ | cut -d'=' -f1)
	for var in $vars ; do
		unset "$var"
	done

	local conf_assignments=""
	if [ -e "$conf_loc" ] ; then
		local line
		conf_assignments=$(conf_assignments_from_file "$conf_loc")
	fi
	eval "$conf_assignments"

	# Initialise configuration to defaults
	[ -z "${KERNELIMG_CONF_do_symlinks:-}" ] && KERNELIMG_CONF_do_symlinks=true
	[ -z "${KERNELIMG_CONF_image_dest:-}" ] && KERNELIMG_CONF_image_dest=/
	[ -z "${KERNELIMG_CONF_link_in_boot:-}" ] && KERNELIMG_CONF_link_in_boot=false
	[ -z "${KERNELIMG_CONF_no_symlinks:-}" ] && KERNELIMG_CONF_no_symlinks=false


	# This is still set (to 0) by default in jessie so we should only
	# warn if the default is changed
	if $KERNELIMG_CONF_no_symlinks ; then
		echo "$conf_loc: W: ignoring no_symlinks; only symlinks are supported" >&2
	fi
	unset KERNELIMG_CONF_no_symlinks

	if $KERNELIMG_CONF_link_in_boot ; then
		KERNELIMG_CONF_image_dest="/boot"
	fi
	unset KERNELIMG_CONF_link_in_boot
}

sort_by_version () {
	local pivot smaller="" larger=""

	IFS="$_newline" read -r pivot
	[ -z "$pivot" ] && return

	local ver
	while IFS="$_newline" read -r ver ; do
		[ -z "$ver" ] && continue # Ignore empty lines.
		if [ "$(version_cmp "$ver" "$pivot")" = "-1" ]; then
			smaller="${smaller}${smaller:+$_newline}${ver}"
		else
			larger="${larger}${larger:+$_newline}${ver}"
		fi
	done

	smaller=$(echo "$smaller" | sort_by_version)
	larger=$(echo "$larger" | sort_by_version)

	if [ -n "$smaller" ] ; then
		echo "$smaller"
	fi
	echo "$pivot"
	if [ -n "$larger" ] ; then
		echo "$larger"
	fi
}
