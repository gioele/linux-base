#!/bin/sh

# Copyright 2016 Ben Hutchings (original Perl script)
# Copyright 2022 Gioele Barabucci

# SPDX-License-Identifier: GPL-2.0-or-later

set -eu

. "$(dirname "$0")/../lib/debian-linux.sh"

# shellcheck disable=SC2034  # Defined here, but used by sorted_images_initrd
LINUX_VERSION="$(dirname "$0")/linux-version"

usage () {
	cat <<EOT
Usage: $0 {install|upgrade|remove} VERSION IMAGE-PATH

This command is intended to be called from the postinst and postrm
maintainer scripts of Linux kernel packages.  The postinst script must
pass the first argument 'install' or 'upgrade' depending on whether a
fresh installation or an upgrade has taken place.

The VERSION argument must be the kernel version string as shown by
'uname -r' and used in filenames.

The IMAGE-PATH argument must be the absolute filename of the kernel
image.
EOT
}

usage_error () {
	usage >&2
	exit 2
}

pretend () {
	local precmd="" fd=1
	if [ -n "${IMAGE_LIST_FILES_IN_BOOT:-}" ] ; then
		precmd="echo"
		fd=2
	fi
	$precmd "$@" 1>&$fd
}

symlink () {
	local source="$1" dest="$2"
	pretend ln -s "$source" "$dest"
}

rename () {
	local source="$1" dest="$2"
	pretend mv "$source" "$dest"
}

unlink_f () {
	local path="$1"
	pretend unlink "$path"
}


update_symlink () {
	local source="$1"
	local dest="$2"

	source=$(readlink_f "$source")

	# Don't make unnecessary changes
	local old_source
	old_source=$(readlink_f "$dest")
	if [ -n "$old_source" ] && [ "$old_source" = "$source" ] ; then
		return
	fi

	# Create a symlink with a temporary name
	local rand
	rand=$(shuf -i 1-999999 -n 1)
	if ! symlink "$source" "$dest.$rand" ; then
		echo "Failed to create symlink to $source"
		exit 1
	fi

	# Move it into place atomically
	if ! rename "$dest.$rand" "$dest" ; then
		unlink_f "$dest.$rand"
	fi

	echo "I: $dest is now a symlink to $source"
}

promote_default () {
	local sorted_images="$1"
	local link="$2"

	# Get the absolute path
	local image_path
	image_path=$(readlink_f "$link")
	if [ -z "${image_path}" ] ; then
		echo "$sorted_images"
		return
	fi

	# If it's already on the list, move it to the end.  (If it's not,
	# presumably the symlink is broken.)
	local i=0
	while read -r img_version img_path img_initrd ; do
		if [ "$img_path" = "$image_path" ] ; then
			sorted_images=$(
				echo "$sorted_images" | head -n "$i"
				echo "$sorted_images" | tail -n $((sorted_images_num - i - 1))
				printf "%s\t%s\t%s\n" "$img_version" "$img_path" "$img_initrd"
			)
			break
		fi
		i=$((i + 1))
	done <<EOV
$sorted_images
EOV

	echo "$sorted_images"
}


update_all () {
	local change="$1"
	local version="$2"
	local image_path="$3"

	# Read /etc/kernelimg.conf and store settings in $KERNELIMG_CONF_*.
	read_kernelimg_conf

	# Check whether symlinks are actually wanted
	# shellcheck disable=SC2154
	if ! $KERNELIMG_CONF_do_symlinks ; then
		exit 0
	fi

	# shellcheck disable=SC2154
	local default_dir="$KERNELIMG_CONF_image_dest"
#    $default_dir =~ s|/*$|/|; # tidy up
	# TODO

	local img_stem
	img_stem=$(image_stem)

	# We build a list of [version, image_path, initrd_path] in
	# ascending priority order, then update the symlinks to match it.

	# Start with a list of existing images sorted by version, adding any
	# existing initrds.
	local sorted_images sorted_images_num
	sorted_images=$(sorted_images_initrd)
	sorted_images_num=$(echo "$sorted_images" | wc -l)

	# The files for this version may not actually have been
	# installed/removed yet (e.g. on installation the initrd will
	# probably be built later).  Also, on installation this version
	# should have the highest priority, not dependent on version
	# ordering.  So ensure that this version is included (for
	# upgrade) or excluded (otherwise).
	local initrd_path="/boot/initrd.img-$version"
	if [ -n "${INITRD:-}" ] && [ "$INITRD" = "No" ] ; then
		initrd_path=""
	fi

	local i=0
	while read -r img_version img_path img_initrd ; do
		local diff="-1"
		[ "$i" -lt "$sorted_images_num" ] && diff=$(version_cmp "$version" "$img_version")

		if [ "$diff" -eq 0 ] ; then
			if [ "$change" = "upgrade" ] ; then
				# Replace image in position $i.
				sorted_images=$(
					echo "$sorted_images" | head -n "$i"
					printf "%s\t%s\t%s\n" "$version" "$image_path" "$initrd_path"
					echo "$sorted_images" | tail -n $((sorted_images_num - i - 1))
				)
			else
				# Remove image in position $i.
				# It may be added back by one of the following steps, if needed.
				sorted_images=$(
					echo "$sorted_images" | head -n "$i"
					echo "$sorted_images" | tail -n $((sorted_images_num - i - 1))
				)
			fi
			break
		elif [ "$diff" -eq -1 ] ; then
			if [ "$change" = "upgrade" ] ; then
				sorted_images=$(
					echo "$sorted_images" | head -n "$i"
					printf "%s\t%s\t%s\n" "$version" "$image_path" "$initrd_path"
					echo "$sorted_images" | tail -n $((sorted_images_num - i))
				)
			fi
			break
		fi
		i=$((i + 1))
	done <<EOV
$sorted_images
EOV

	# Any existing non-broken symlinks have higher priority.
	sorted_images=$(promote_default "$sorted_images" "${default_dir}${img_stem}.old")
	sorted_images=$(promote_default "$sorted_images" "${default_dir}${img_stem}")

	# In case of installation, this version has highest priority.
	if [ "$change" = "install" ] ; then
		sorted_images=$(
			echo "$sorted_images"
			printf "%s\t%s\t%s\n" "$version" "$image_path" "$initrd_path"
		)
	fi

	if [ -n "$sorted_images" ] ; then
		# Take the two highest priority entries on the list.  In case there
		# is only one entry, use it twice!

		sorted_images_num=$(echo "$sorted_images" | wc -l)
		local sorted_images_rev
		sorted_images_rev=$(echo "$sorted_images" | tac)
		local line1 line2
		line1=$(echo "$sorted_images_rev" | head -n 1)
		if [ "$sorted_images_num" -ge 2 ] ; then
			line2=$(echo "$sorted_images_rev" | head -n 2 | tail -n 1)
		else
			line2="$line1"
		fi

		local old_image_path old_initrd_path
		old_image_path=$(echo "$line2" | cut -f2)
		old_initrd_path=$(echo "$line2" | cut -f3)

		local cur_image_path cur_initrd_path
		cur_image_path=$(echo "$line1" | cut -f2)
		cur_initrd_path=$(echo "$line1" | cut -f3)

		update_symlink "$old_image_path" "${default_dir}${img_stem}.old"
		if [ -n "$old_initrd_path" ] ; then
			update_symlink "$old_initrd_path" "${default_dir}initrd.img.old"
		else
			unlink_f "${default_dir}initrd.img.old"
		fi

		update_symlink "$cur_image_path" "${default_dir}${img_stem}"
		if [ -n "$cur_initrd_path" ] ; then
			update_symlink "$cur_initrd_path" "${default_dir}initrd.img"
		else
			unlink_f "${default_dir}initrd.img"
		fi
	else
		echo "W: Last kernel image has been removed, so removing the default symlinks" >&2
		unlink_f "${default_dir}${img_stem}.old"
		unlink_f "${default_dir}initrd.old"
		unlink_f "${default_dir}${img_stem}"
		unlink_f "${default_dir}initrd"
	fi

	exit 0
}

if [ $# -eq 0 ] ; then
	usage_error
fi

case "$1" in
	help) usage ; exit 0 ;;
	install|upgrade|remove)
		[ $# -eq 3 ] || usage_error
		update_all "$@"
		;;
esac

usage_error
