#!/bin/sh

# Copyright 2011 Ben Hutchings (original Perl script)
# Copyright 2022 Gioele Barabucci

# SPDX-License-Identifier: GPL-2.0-or-later

set -eu

. "$(dirname "$0")/../lib/debian-linux.sh"

usage () {
	cat <<EOT
Usage: $0 compare VERSION1 OP VERSION2
       $0 sort [--reverse] [VERSION1 VERSION2 ...]
       $0 list [--paths]

The version arguments should be kernel version strings as shown by
'uname -r' and used in filenames.

The valid comparison operators are: lt le eq ge gt
EOT
}

usage_error () {
	usage >&2
	exit 2
}

compare_versions () {
	# Check arguments
	[ $# -ne 3 ] && usage_error

	local left=$1
	local op=$2
	local right=$3

	local op_shell
	case $op in
		lt|le|eq|ge|gt) op_shell="-${op}" ;;
		*) usage_error ;;
	esac

	sign=$(version_cmp $left $right)
	# shellcheck disable=SC1009,SC1072,SC1073
	if [ "$sign" $op_shell 0 ] ; then
		exit 0
	else
		exit 1
	fi
}

sort_versions () {
	# Check for --reverse option
	local reverse=false
	if [ $# -ge 1 ] && [ $1 = '--reverse' ] ; then
		reverse=true
		shift
	fi

	# Collect versions from argv or stdin (with optional suffix after a space)
	local versions=""
	if [ $# -ge 1 ] ; then
		local v
		for v ; do
			versions="${versions}${versions:+$_newline}${v}"
		done
	else
		local line
		while IFS="$_newline" read -r line ; do
			versions="${versions}${versions:+$_newline}${line}"
		done
	fi

	sorted_vers=$(echo "$versions" | sort_by_version)
	if $reverse ; then
		echo "$sorted_vers" | tac
	else
		echo "$sorted_vers"
	fi

	exit 0
}


list_versions () {
	local show_paths=false
	if [ $# -eq 1 ] && [ "$1" = "--paths" ] ; then
		show_paths=true
	elif [ $# -ne 0 ] ; then
		usage_error
	fi

	local list=$(image_list)
	if $show_paths ; then
		echo "$list" | tr '\t' ' '
	else
		echo "$list" | cut -f1
	fi

	exit 0
}

if [ $# -eq 0 ] ; then
	usage_error
fi

cmd=$1
shift

case $cmd in
	help|TODO) usage ; exit 0 ;;
	compare) compare_versions $* ;;
	sort) sort_versions $* ;;
	list) list_versions $* ;;
esac

usage_error
