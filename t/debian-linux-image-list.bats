#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
        [ "$1" = "$2" ] && return 0

        echo "expected '$2', got '$1'" >&2
        return 1
}

## image_list

@test "image_list amd64" {
	IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/ipxe.efi
	/boot/initrd.img-4.19.0-4-amd64
	/boot/lost+found
	/boot/..
	/boot/System.map-4.19.0-4-amd64
	/boot/config-4.19.0-3-amd64
	/boot/vmlinuz-4.19.0-4-amd64
	/boot/initrd.img-4.19.0-3-amd64
	/boot/grub
	/boot/vmlinuz-4.19.0-3-amd64
	/boot/efi /boot/ipxe.lkrn
	/boot/.
	/boot/config-4.19.0-4-amd64
	/boot/System.map-4.19.0-3-amd64
	EOF
	)

	local list=$(image_list)
	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | tail -n1)

	assert_equal $(echo "$list" | wc -l)  "2"
	assert_equal "$line1" "$(printf '4.19.0-4-amd64\t/boot/vmlinuz-4.19.0-4-amd64')"
	assert_equal "$line2" "$(printf '4.19.0-3-amd64\t/boot/vmlinuz-4.19.0-3-amd64')"
}


@test "image_list ppc64le" {
	IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/..
	/boot/vmlinux /boot/vmlinux.old
	/boot/initrd.img-4.9.0-7-powerpc64le
	/boot/vmlinux-4.9.0-7-powerpc64le
	/boot/System.map-4.9.0-7-powerpc64le
	/boot/initrd.img.old
	/boot/System.map-4.9.0-6-powerpc64le
	/boot/.
	/boot/config-4.9.0-7-powerpc64le
	/boot/System.map-4.9.0-8-powerpc64le
	/boot/vmlinux-4.9.0-8-powerpc64le
	/boot/initrd.img-4.9.0-6-powerpc64le
	/boot/vmlinux-4.9.0-6-powerpc64le
	/boot/initrd.img-4.9.0-8-powerpc64le
	/boot/config-4.9.0-8-powerpc64le
	/boot/grub
	/boot/initrd.img
	/boot/config-4.9.0-6-powerpc64le
	EOF
	)

	local list=$(image_list)
	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)
	local line3=$(echo "$list" | head -n3 | tail -n1)

	assert_equal $(echo "$list" | wc -l)  "3"
	assert_equal "$line1" "$(printf '4.9.0-7-powerpc64le\t/boot/vmlinux-4.9.0-7-powerpc64le')"
	assert_equal "$line2" "$(printf '4.9.0-8-powerpc64le\t/boot/vmlinux-4.9.0-8-powerpc64le')"
	assert_equal "$line3" "$(printf '4.9.0-6-powerpc64le\t/boot/vmlinux-4.9.0-6-powerpc64le')"
}


@test "image_list m68k" {
	IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinux-4.19.0-3-m68k
	/boot/vmlinux-4.19.0-4-m68k
	/boot/vmlinuz-4.1.0-2-m68k
	/boot/config-4.19.0-3-m68k
	/boot/config-4.19.0-4-m68k
	/boot/config-4.1.0-2-m68k
	/boot/initrd.img-4.19.0-3-m68k
	/boot/initrd.img-4.19.0-4-m68k
	/boot/initrd.img-4.1.0-2-m68k
	/boot/System.map-4.19.0-3-m68k
	/boot/System.map-4.19.0-4-m68k
	/boot/System.map-4.1.0-2-m68k
	EOF
	)

	local list=$(image_list)
	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)
	local line3=$(echo "$list" | head -n3 | tail -n1)

	assert_equal $(echo "$list" | wc -l)  "3"
	assert_equal "$line1" "$(printf '4.19.0-3-m68k\t/boot/vmlinux-4.19.0-3-m68k')"
	assert_equal "$line2" "$(printf '4.19.0-4-m68k\t/boot/vmlinux-4.19.0-4-m68k')"
	assert_equal "$line3" "$(printf '4.1.0-2-m68k\t/boot/vmlinuz-4.1.0-2-m68k')"
}

@test "image_list sig files" {
	IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/config-4.16.0-2-amd64
	/boot/config-4.17.0-1-amd64
	/boot/initrd.img-4.16.0-2-amd64
	/boot/initrd.img-4.17.0-1-amd64
	/boot/System.map-4.16.0-2-amd64
	/boot/System.map-4.17.0-1-amd64
	/boot/vmlinuz-4.16.0-2-amd64
	/boot/vmlinuz-4.16.0-2-amd64.sig
	/boot/vmlinuz-4.17.0-1-amd64
	/boot/vmlinuz-4.17.0-1-amd64.sig
	EOF
	)

	local list=$(image_list)
	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)

	assert_equal $(echo "$list" | wc -l)  "2"
	assert_equal "$line1" "$(printf '4.16.0-2-amd64\t/boot/vmlinuz-4.16.0-2-amd64')"
	assert_equal "$line2" "$(printf '4.17.0-1-amd64\t/boot/vmlinuz-4.17.0-1-amd64')"
}
