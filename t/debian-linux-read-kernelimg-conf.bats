#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
	[ "$1" = "$2" ] && return 0

	echo "expected '$2', got '$1'" >&2
	return 1
}

## read_kernelimg_conf

## Empty config
@test "read_kernelimg_conf empty config" {
        local conf_file="$BATS_RUN_TMPDIR/rkc-empty-config.conf"
	touch "$conf_file"

	read_kernelimg_conf "$conf_file"

	assert_equal "$KERNELIMG_CONF_do_symlinks" "true"
	assert_equal "$KERNELIMG_CONF_image_dest" "/"
	assert_equal $(set | grep ^KERNELIMG_CONF_ | wc -l) "2"
}

## Sample config
@test "read_kernelimg_conf sample config" {
        local conf_file="$BATS_RUN_TMPDIR/rkc-sample-config.conf"
	cat >"$conf_file" <<EOT
# This is a sample /etc/kernel-img.conf file
# See kernel-img.conf(5) for details

# If you want the symbolic link (or image, if move_image is set) to be
# stored elsewhere than / set this variable to the dir where you
# want the symbolic link.  Please note that this is not a Boolean
# variable.  This may be of help to loadlin users, who may set both
# this and move_image. Defaults to /. This can be used in conjunction
# with all above options except link_in_boot, which would not make
# sense.  (If both image_dest and link_in_boot are set, link_in_boot
# overrides).
image_dest = /

# This option manipulates the build link created by recent kernels. If
# the link is a dangling link, and if a the corresponding kernel
# headers appear to have been installed on the system, a new symlink
# shall be created to point to them.
#relink_build_link = YES

# If set, the preinst shall silently try to move /lib/modules/version
# out of the way if it is the same version as the image being
# installed. Use at your own risk.
#clobber_modules = NO

# If set, does not prompt to continue after a depmod problem in the
# postinstall script.  This facilitates automated installs, though it
# may mask a problem with the kernel image. A diag‐ nostic is still
# issued. This is unset be default.
# ignore_depmod_err = NO

# These setting are for legacy postinst scripts only. newer postinst
# scripts from the kenrel-package do not use them
do_symlinks = yes
do_bootloader = no
do_initrd=yes
link_in_boot=no
EOT
	read_kernelimg_conf "$conf_file"

	assert_equal "$KERNELIMG_CONF_do_symlinks" "true"
	assert_equal "$KERNELIMG_CONF_image_dest" "/"
	assert_equal $(set | grep ^KERNELIMG_CONF_ | wc -l) "2"
}

## Slightly different spacing and value syntax
@test "read_kernelimg_conf different spacing and syntax" {
        local conf_file="$BATS_RUN_TMPDIR/rkc-spacing-syntax.conf"
	cat >"$conf_file" <<EOT
image_dest = foo bar
	relink_build_link = yes
do_symlinks = 0
    link_in_boot= false
no_symlinks=1
EOT

	read_kernelimg_conf "$conf_file"

	assert_equal "$KERNELIMG_CONF_do_symlinks" "false"
	assert_equal "$KERNELIMG_CONF_image_dest" "foo"
	assert_equal $(set | grep ^KERNELIMG_CONF_ | wc -l) "2"
}

## Check that 'false' and 'no' also work
@test "read_kernelimg_conf false=false" {
        local conf_file="$BATS_RUN_TMPDIR/rkc-false-false.conf"
	cat >"$conf_file" <<EOT
do_symlinks = false
EOT
	read_kernelimg_conf "$conf_file"

	assert_equal "$KERNELIMG_CONF_do_symlinks" "false"
	assert_equal "$KERNELIMG_CONF_image_dest" "/"
	assert_equal $(set | grep ^KERNELIMG_CONF_ | wc -l) "2"
}

@test "read_kernelimg_conf no=false" {
        local conf_file="$BATS_RUN_TMPDIR/rkc-false-no.conf"
	cat >"$conf_file" <<EOT
do_symlinks = no
EOT
	read_kernelimg_conf "$conf_file"

	assert_equal "$KERNELIMG_CONF_do_symlinks" "false"
	assert_equal "$KERNELIMG_CONF_image_dest" "/"
	assert_equal $(set | grep ^KERNELIMG_CONF_ | wc -l) "2"
}

## Check that invalid values have no effect
@test "read_kernelimg_conf invalid values" {
        local conf_file="$BATS_RUN_TMPDIR/rkc-invalid-values.conf"
	cat >"$conf_file" <<EOT
do_symlinks=
link_in_boot yes
link_in_boot 1
EOT
	read_kernelimg_conf "$conf_file"

	assert_equal "$KERNELIMG_CONF_do_symlinks" "true"
	assert_equal "$KERNELIMG_CONF_image_dest" "/"
	assert_equal $(set | grep ^KERNELIMG_CONF_ | wc -l) "2"
}

## Check link_in_boot dominates image_dest
@test "read_kernelimg_conf link_in_boot" {
        local conf_file="$BATS_RUN_TMPDIR/rkc-link-in-boot.conf"
	cat >"$conf_file" <<EOT
image_dest = /local
link_in_boot = true
EOT
	read_kernelimg_conf "$conf_file"

	assert_equal "$KERNELIMG_CONF_do_symlinks" "true"
	assert_equal "$KERNELIMG_CONF_image_dest" "/boot"
	assert_equal $(set | grep ^KERNELIMG_CONF_ | wc -l) "2"
}
