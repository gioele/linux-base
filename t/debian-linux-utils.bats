#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
	[ "$1" = "$2" ] && return 0

	echo "expected '$2', got '$1'" >&2
	return 1
}

## is_num

@test "is_num 0" { is_num "0" ; }
@test "is_num 4" { is_num "4" ; }
@test "is_num 12" { is_num "12" ; }
@test "is_num 382" { is_num "382" ; }
@test "is_num 07" { is_num "07" ; }

@test "is_num -10" { ! is_num "-10" ; }
@test "is_num +85" { ! is_num "+85" ; }
@test "is_num amd64" { ! is_num "amd64" ; }
@test "is_num 32e" { ! is_num "32e" ; }
@test "is_num 2e8" { ! is_num "2e8" ; }
