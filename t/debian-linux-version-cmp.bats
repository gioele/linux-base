#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
	[ "$1" = "$2" ] && return 0

	echo "expected '$2', got '$1'" >&2
	return 1
}

## Simple numeric comparison

@test "cmp 2 2" { assert_equal "$(version_cmp '2' '2')" "0" ; }
@test "cmp 2 3" { assert_equal "$(version_cmp '2' '3')" "-1" ; }
@test "cmp 3 2" { assert_equal "$(version_cmp '3' '2')" "1" ; }

## Multiple components
@test "cmp 2.6.32 2.6.32" { assert_equal "$(version_cmp '2.6.32' '2.6.32')" "0" ; }
@test "cmp 2.6.32 2.6.33" { assert_equal "$(version_cmp '2.6.32' '2.6.33')" "-1" ; }
@test "cmp 2.6.33 2.6.32" { assert_equal "$(version_cmp '2.6.33' '2.6.32')" "1" ; }

## Extra components (non-numeric, non-pre-release) > null
@test "cmp 2.6.32-local 2.6.32-local" { assert_equal "$(version_cmp '2.6.32-local' '2.6.32-local')" "0" ; }
@test "cmp 2.6.32 2.6.32-local" { assert_equal "$(version_cmp '2.6.32' '2.6.32-local')" "-1" ; }
@test "cmp 2.6.32-local 2.6.32" { assert_equal "$(version_cmp '2.6.32-local' '2.6.32')" "1" ; }

## Extra numeric components > null
@test "cmp 2.6.32 2.6.32.1" { assert_equal "$(version_cmp '2.6.32' '2.6.32.1')" "-1" ; }
@test "cmp 2.6.32.1 2.6.32" { assert_equal "$(version_cmp '2.6.32.1' '2.6.32')" "1" ; }
@test "cmp 2.6.32 2.6.32-1" { assert_equal "$(version_cmp '2.6.32' '2.6.32-1')" "-1" ; }
@test "cmp 2.6.32-1 2.6.32" { assert_equal "$(version_cmp '2.6.32-1' '2.6.32')" "1" ; }

## Extra pre-release components < null
@test "cmp 2.6.33-rc1 2.6.33-rc1" { assert_equal "$(version_cmp '2.6.33-rc1' '2.6.33-rc1')" "0" ; }
@test "cmp 2.6.33-rc1 2.6.33" { assert_equal "$(version_cmp '2.6.33-rc1' '2.6.33')" "-1" ; }
@test "cmp 2.6.33 2.6.33-rc1" { assert_equal "$(version_cmp '2.6.33' '2.6.33-rc1')" "1" ; }
@test "cmp 2.6.33-trunk 2.6.33-trunk" { assert_equal "$(version_cmp '2.6.33-trunk' '2.6.33-trunk')" "0" ; }
@test "cmp 2.6.33-rc1 2.6.33-trunk" { assert_equal "$(version_cmp '2.6.33-rc1' '2.6.33-trunk')" "-1" ; }
@test "cmp 2.6.33-trunk 2.6.33" { assert_equal "$(version_cmp '2.6.33-trunk' '2.6.33')" "-1" ; }

## Pre-release < numeric
@test "cmp 2.6.32-1 2.6.32-trunk" { assert_equal "$(version_cmp '2.6.32-1' '2.6.32-trunk')" "1" ; }
@test "cmp 2.6.32-trunk 2.6.32-1" { assert_equal "$(version_cmp '2.6.32-trunk' '2.6.32-1')" "-1" ; }

## Pre-release < non-numeric non-pre-release
@test "cmp 2.6.32-local 2.6.32-trunk" { assert_equal "$(version_cmp '2.6.32-local' '2.6.32-trunk')" "1" ; }
@test "cmp 2.6.32-trunk 2.6.32-local" { assert_equal "$(version_cmp '2.6.32-trunk' '2.6.32-local')" "-1" ; }

## Pre-release cases including flavour (#761614)
@test "cmp 2.6.33-trunk-flavour 2.6.33-trunk-flavour" { assert_equal "$(version_cmp '2.6.33-trunk-flavour' '2.6.33-trunk-flavour')" "0" ; }
@test "cmp 2.6.33-rc1 2.6.33-trunk-flavour" { assert_equal "$(version_cmp '2.6.33-rc1' '2.6.33-trunk-flavour')" "-1" ; }
@test "cmp 2.6.33-rc1-flavour 2.6.33-trunk-flavour" { assert_equal "$(version_cmp '2.6.33-rc1-flavour' '2.6.33-trunk-flavour')" "-1" ; }
@test "cmp 2.6.32-1-flavour 2.6.32-trunk-flavour" { assert_equal "$(version_cmp '2.6.32-1-flavour' '2.6.32-trunk-flavour')" "1" ; }
@test "cmp 2.6.32-trunk-flavour 2.6.32-1-flavour" { assert_equal "$(version_cmp '2.6.32-trunk-flavour' '2.6.32-1-flavour')" "-1" ; }
@test "cmp 2.6.32-local 2.6.32-trunk-flavour" { assert_equal "$(version_cmp '2.6.32-local' '2.6.32-trunk-flavour')" "1" ; }
@test "cmp 2.6.32-trunk-flavour 2.6.32-local" { assert_equal "$(version_cmp '2.6.32-trunk-flavour' '2.6.32-local')" "-1" ; }

## Numeric < non-numeric non-pre-release
@test "cmp 2.6.32-1 2.6.32-local" { assert_equal "$(version_cmp '2.6.32-1' '2.6.32-local')" "-1" ; }
@test "cmp 2.6.32-local 2.6.32-1" { assert_equal "$(version_cmp '2.6.32-local' '2.6.32-1')" "1" ; }

## Hyphen < dot
@test "cmp 2.6.32-2 2.6.32.1" { assert_equal "$(version_cmp '2.6.32-2' '2.6.32.1')" "-1" ; }
@test "cmp 2.6.32.1 2.6.32-2" { assert_equal "$(version_cmp '2.6.32.1' '2.6.32-2')" "1" ; }
