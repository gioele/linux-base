#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
        [ "$1" = "$2" ] && return 0

        echo "expected '$2', got '$1'" >&2
        return 1
}

## readlink_f

@test "readlink_f (link present)" {
	READLINK_LINKS=$(cat <<-EOF
	/boot/a	/boot/b
	/vmlinuz	/boot/another-file
	EOF
	)

	assert_equal "$(readlink_f '/vmlinuz')" "/boot/another-file"
}

@test "readlink_f (not a link)" {
	READLINK_LINKS=$(cat <<-EOF
	/boot/a	/boot/b
	EOF
	)

	assert_equal "$(readlink_f '/vmlinuz')" "/vmlinuz"
}

@test "readlink_f (empty param)" {
	assert_equal "$(readlink_f '')" ""
}
