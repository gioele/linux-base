#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
        [ "$1" = "$2" ] && return 0

        echo "expected '$2', got '$1'" >&2
        return 1
}

## sort_by_version

@test "sort_by_version 1" {
	local v vers="2.3"
	local sorted=$(for v in $vers ; do echo "$v" ; done | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "1"
	assert_equal "$(echo "$sorted" | tr '\n' ' ')" "2.3 "
}

@test "sort_by_version 2" {
	local v vers="2.3 2.1"
	local sorted=$(for v in $vers ; do echo "$v" ; done | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "2"
	assert_equal "$(echo "$sorted" | tr '\n' ' ')" "2.1 2.3 "
}

@test "sort_by_version 3" {
	local v vers="2.3 2.1 3.0"
	local sorted=$(for v in $vers ; do echo "$v" ; done | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "3"
	assert_equal "$(echo "$sorted" | tr '\n' ' ')" "2.1 2.3 3.0 "
}

@test "sort_by_version 4" {
	local v vers="2.3 2.1 3.0 1.2"
	local sorted=$(for v in $vers ; do echo "$v" ; done | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "4"
	assert_equal "$(echo "$sorted" | tr '\n' ' ')" "1.2 2.1 2.3 3.0 "
}

@test "sort_by_version 5" {
	local v vers="2.3 2.1 3.0 2.5 1.2"
	local sorted=$(for v in $vers ; do echo "$v" ; done | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "5"
	assert_equal "$(echo "$sorted" | tr '\n' ' ')" "1.2 2.1 2.3 2.5 3.0 "
}

@test "sort_by_version 5 (in order)" {
	local v vers="1.2 2.1 2.3 2.5 3.0"
	local sorted=$(for v in $vers ; do echo "$v" ; done | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "5"
	assert_equal "$(echo "$sorted" | tr '\n' ' ')" "1.2 2.1 2.3 2.5 3.0 "
}

@test "sort_by_version 5 (in reverse order)" {
	local v vers="1.2 2.1 2.3 2.5 3.0"
	local sorted=$(for v in $vers ; do echo "$v" ; done | tac | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "5"
	assert_equal "$(echo "$sorted" | tr '\n' ' ')" "1.2 2.1 2.3 2.5 3.0 "
}


@test "sort_by_version 5 (with comments)" {
	local v vers="2.3 2.1 3.0 2.5 1.2"
	local sorted=$(for v in $vers ; do printf "%s\t%s\n" "$v" "x$v" ; done | sort_by_version)

	assert_equal "$(echo "$sorted" | wc -l)" "5"
	assert_equal "$(echo "$sorted" | tr '\t\n' '_ ')" "1.2_x1.2 2.1_x2.1 2.3_x2.3 2.5_x2.5 3.0_x3.0 "
}

