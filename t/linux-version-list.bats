#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
        [ "$1" = "$2" ] && return 0

        echo "expected '$2', got '$1'" >&2
        return 1
}

assert_contains_line () {
	echo "$1" | grep "$2" && return 0

	echo "expected one line of '$1' to match '$2'" >&2
	return 1
}

## linux-version list

LINUX_VERSION="$BATS_TEST_DIRNAME/../bin/linux-version"

@test "linux-version list skip others" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/ipxe.efi
	/boot/initrd.img-4.19.0-4-amd64
	/boot/lost+found
	/boot/..
	/boot/System.map-4.19.0-4-amd64
	/boot/config-4.19.0-3-amd64
	/boot/vmlinuz-4.19.0-4-amd64
	/boot/initrd.img-4.19.0-3-amd64
	/boot/grub
	/boot/vmlinuz-4.19.0-3-amd64
	/boot/efi /boot/ipxe.lkrn
	/boot/.
	/boot/config-4.19.0-4-amd64
	/boot/System.map-4.19.0-3-amd64
	EOF
	)

	local list_fn=$(image_list)
	local list_bin=$($LINUX_VERSION list)

	unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal $(echo "$list_bin" | wc -l ) $(echo "$list_fn" | wc -l)
	assert_contains_line "$list_bin" '^4.19.0-4-amd64$'
}

@test "linux-version list --paths" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/ipxe.efi
	/boot/initrd.img-4.19.0-4-amd64
	/boot/lost+found
	/boot/..
	/boot/System.map-4.19.0-4-amd64
	/boot/config-4.19.0-3-amd64
	/boot/vmlinuz-4.19.0-4-amd64
	/boot/initrd.img-4.19.0-3-amd64
	/boot/grub
	/boot/vmlinuz-4.19.0-3-amd64
	/boot/efi /boot/ipxe.lkrn
	/boot/.
	/boot/config-4.19.0-4-amd64
	/boot/System.map-4.19.0-3-amd64
	EOF
	)

	local list_fn=$(image_list)
	local list_bin=$($LINUX_VERSION list --paths)

	unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal $(echo "$list_bin" | wc -l ) $(echo "$list_fn" | wc -l)
	assert_contains_line "$list_bin" '^4.19.0-4-amd64 /boot/vmlinuz-4.19.0-4-amd64$'
	assert_contains_line "$list_bin" '^4.19.0-3-amd64 /boot/vmlinuz-4.19.0-3-amd64$'
}
