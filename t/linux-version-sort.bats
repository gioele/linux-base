#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
        [ "$1" = "$2" ] && return 0

        echo "expected '$2', got '$1'" >&2
        return 1
}

assert_contains_line () {
	echo "$1" | grep "$2" && return 0

	echo "expected one line of '$1' to match '$2'" >&2
	return 1
}

## linux-version sort

LINUX_VERSION="$BATS_TEST_DIRNAME/../bin/linux-version"

@test "linux-version sort 1" {
	local vers="4.19.0-3-amd64"
	local list=$($LINUX_VERSION sort $vers)

	local line1=$(echo "$list" | head -n1)

	assert_equal $(echo "$list" | wc -l ) "1"
	assert_equal "$line1" "4.19.0-3-amd64"
}

@test "linux-version sort 2" {
	local vers="4.19.0-3-amd64 4.10.1-5-amd64"
	local list=$($LINUX_VERSION sort $vers)

	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)

	assert_equal $(echo "$list" | wc -l ) "2"
	assert_equal "$line1" "4.10.1-5-amd64"
	assert_equal "$line2" "4.19.0-3-amd64"
}

@test "linux-version sort 3" {
	local vers="4.19.0-3-amd64 4.10.1-5-amd64 4.19.0-4-amd64"
	local list=$($LINUX_VERSION sort $vers)

	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)
	local line3=$(echo "$list" | head -n3 | tail -n1)

	assert_equal $(echo "$list" | wc -l ) "3"
	assert_equal "$line1" "4.10.1-5-amd64"
	assert_equal "$line2" "4.19.0-3-amd64"
	assert_equal "$line3" "4.19.0-4-amd64"
}

@test "linux-version sort 3 --reverse" {
	local vers="4.19.0-3-amd64 4.10.1-5-amd64 4.19.0-4-amd64"
	local list=$($LINUX_VERSION sort --reverse $vers)

	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)
	local line3=$(echo "$list" | head -n3 | tail -n1)

	assert_equal $(echo "$list" | wc -l ) "3"
	assert_equal "$line1" "4.19.0-4-amd64"
	assert_equal "$line2" "4.19.0-3-amd64"
	assert_equal "$line3" "4.10.1-5-amd64"
}

@test "linux-version sort 5 (stdin)" {
	local vers="4.19.0-3-amd64 4.10.1-5-amd64 4.19.0-4-amd64 3.2-rc1 5.0-rc1"
	local list=$(for v in $vers ; do echo "$v" ; done | $LINUX_VERSION sort)

	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)
	local line3=$(echo "$list" | head -n3 | tail -n1)
	local line4=$(echo "$list" | head -n4 | tail -n1)
	local line5=$(echo "$list" | head -n5 | tail -n1)

	assert_equal $(echo "$list" | wc -l ) "5"
	assert_equal "$line1" "3.2-rc1"
	assert_equal "$line2" "4.10.1-5-amd64"
	assert_equal "$line3" "4.19.0-3-amd64"
	assert_equal "$line4" "4.19.0-4-amd64"
	assert_equal "$line5" "5.0-rc1"
}

@test "linux-version sort with paths (stdin)" {
	local vers="4.19.0-3-amd64 4.10.1-5-amd64 4.19.0-4-amd64 3.2-rc1 5.0-rc1"
	local list=$(for v in $vers ; do echo "$v /boot/vmlinuz-$v" ; done | $LINUX_VERSION sort)

	local line1=$(echo "$list" | head -n1)
	local line2=$(echo "$list" | head -n2 | tail -n1)
	local line3=$(echo "$list" | head -n3 | tail -n1)
	local line4=$(echo "$list" | head -n4 | tail -n1)
	local line5=$(echo "$list" | head -n5 | tail -n1)

	assert_equal $(echo "$list" | wc -l ) "5"
	assert_equal "$line1" "3.2-rc1 /boot/vmlinuz-3.2-rc1"
	assert_equal "$line2" "4.10.1-5-amd64 /boot/vmlinuz-4.10.1-5-amd64"
	assert_equal "$line3" "4.19.0-3-amd64 /boot/vmlinuz-4.19.0-3-amd64"
	assert_equal "$line4" "4.19.0-4-amd64 /boot/vmlinuz-4.19.0-4-amd64"
	assert_equal "$line5" "5.0-rc1 /boot/vmlinuz-5.0-rc1"
}
