#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
	[ "$1" = "$2" ] && return 0

	echo "expected '$2', got '$1'" >&2
	return 1
}

assert_contains_line () {
	echo "$1" | grep "$2" && return 0

	echo "expected one line of '$1' to match '$2'" >&2
	return 1
}

## sorted_images_initrd

LINUX_VERSION="$BATS_TEST_DIRNAME/../bin/linux-version"

@test "sorted_images_initrd (no initrd)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	EOF
	)

        local list=$(sorted_images_initrd)
        local line1=$(echo "$list" | head -n1)
        local line2=$(echo "$list" | head -n2 | tail -n1)

	echo "$list"
        assert_equal $(echo "$list" | wc -l)  "2"
        assert_equal "$line1" "$(printf '4.11.0-1-amd64\t/boot/vmlinuz-4.11.0-1-amd64\t')"
        assert_equal "$line2" "$(printf '4.33.0-3-amd64\t/boot/vmlinuz-4.33.0-3-amd64\t')"
}

@test "sorted_images_initrd (some initrd)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	EOF
	)

        local list=$(sorted_images_initrd)
        local line1=$(echo "$list" | head -n1)
        local line2=$(echo "$list" | head -n2 | tail -n1)

	echo "$list"
        assert_equal $(echo "$list" | wc -l)  "2"
        assert_equal "$line1" "$(printf '4.11.0-1-amd64\t/boot/vmlinuz-4.11.0-1-amd64\t')"
        assert_equal "$line2" "$(printf '4.33.0-3-amd64\t/boot/vmlinuz-4.33.0-3-amd64\t/boot/initrd.img-4.33.0-3-amd64')"
}

@test "sorted_images_initrd (initrd)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	EOF
	)

        local list=$(sorted_images_initrd)
        local line1=$(echo "$list" | head -n1)
        local line2=$(echo "$list" | head -n2 | tail -n1)

	echo "$list"
        assert_equal $(echo "$list" | wc -l)  "2"
        assert_equal "$line1" "$(printf '4.11.0-1-amd64\t/boot/vmlinuz-4.11.0-1-amd64\t/boot/initrd.img-4.11.0-1-amd64')"
        assert_equal "$line2" "$(printf '4.33.0-3-amd64\t/boot/vmlinuz-4.33.0-3-amd64\t/boot/initrd.img-4.33.0-3-amd64')"
}
