#!/usr/bin/env bats

load ../lib/debian-linux.sh

assert_equal () {
	[ "$1" = "$2" ] && return 0

	echo "expected '$2', got '$1'" >&2
	return 1
}

assert_contains_line () {
	echo "$1" | grep "$2" && return 0

	echo "expected one line of '$1' to match '$2'" >&2
	return 1
}

assert_not_contains_line () {
	echo "$1" | grep "$2" && return 1 || return 0

	echo "expected no lines of '$1' to match '$2'" >&2
	return 1
}

## linux-update-symlinks install

LINUX_UPDATE_SYMLINKS="$BATS_TEST_DIRNAME/../bin/linux-update-symlinks"

@test "linux-update-symlinks install (first)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.22.0-2-amd64
	/boot/initrd.img-4.22.0-2-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS install 4.11.0-1-amd64 /boot/vmlinuz-4.11.0-1-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.11.0-1-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks install (middle)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS install 4.22.0-2-amd64 /boot/vmlinuz-4.22.0-2-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.22.0-2-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks install (middle, non-last current image)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	export READLINK_LINKS=$(cat <<-EOF
	/vmlinuz	/boot/vmlinuz-4.33.0-3-amd64
	/vmlinuz.old	/boot/vmlinuz-4.11.0-1-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS install 4.22.0-2-amd64 /boot/vmlinuz-4.22.0-2-amd64
        unset IMAGE_LIST_FILES_IN_BOOT
	unset READLINK_LINKS

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.22.0-2-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks install (last, new)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS install 4.55.0-5-amd64 /boot/vmlinuz-4.55.0-5-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.55.0-5-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks install (last, replace but different)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS install 4.44.0-4-amd64 /boot/vmlinuz-4.44.0-4-amd64-NEW
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64-NEW /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.old.[0-9]"
}

## linux-update-symlinks upgrade

@test "linux-update-symlinks upgrade (first, replace but different)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.22.0-2-amd64
	/boot/initrd.img-4.22.0-2-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS upgrade 4.11.0-1-amd64 /boot/vmlinuz-4.11.0-1-amd64-SPECIAL
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks upgrade (last, replace but different)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.22.0-2-amd64
	/boot/initrd.img-4.22.0-2-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS upgrade 4.44.0-4-amd64 /boot/vmlinuz-4.44.0-4-amd64-SPECIAL
        unset IMAGE_LIST_FILES_IN_BOOT
	unset READLINK_LINKS

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64-SPECIAL /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/initrd.img-4.44.0-4-amd64 /initrd.img.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks upgrade (middle, replace different, preserve links)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.22.0-2-amd64
	/boot/initrd.img-4.22.0-2-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	# Do not create links for initrd.img{,.old}, so their creation can be checked later.
	export READLINK_LINKS=$(cat <<-EOF
	/vmlinuz	/boot/vmlinuz-4.44.0-4-amd64
	/vmlinuz.old	/boot/vmlinuz-4.33.0-3-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS upgrade 4.22.0-2-amd64 /boot/vmlinuz-4.22.0-2-amd64
        unset IMAGE_LIST_FILES_IN_BOOT
	unset READLINK_LINKS

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/initrd.img-4.44.0-4-amd64 /initrd.img.[0-9]"
	assert_not_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/initrd.img-4.33.0-3-amd64 /initrd.img.old.[0-9]"
	assert_not_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks upgrade (middle, replace same)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.22.0-2-amd64
	/boot/initrd.img-4.22.0-2-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	export READLINK_LINKS=$(cat <<-EOF
	/vmlinuz	/boot/vmlinuz-4.22.0-2-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS upgrade 4.22.0-2-amd64 /boot/vmlinuz-4.22.0-2-amd64
        unset IMAGE_LIST_FILES_IN_BOOT
	unset READLINK_LINKS

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.old.[0-9]"
	assert_not_contains_line "$output" "^ln -s /boot.* /vmlinuz.[0-9]"
	#  TODO
}


@test "linux-update-symlinks upgrade (middle, new)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS upgrade 4.22.0-2-amd64 /boot/vmlinuz-4.22.0-2-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.old.[0-9]"
}

## linux-update-symlinks remove

@test "linux-update-symlinks remove (3 left)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	/boot/vmlinuz-4.44.0-4-amd64
	/boot/initrd.img-4.44.0-4-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS remove 4.22.0-2-amd64 /boot/vmlinuz-4.22.0-2-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.44.0-4-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks remove (2 left)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.11.0-1-amd64
	/boot/initrd.img-4.11.0-1-amd64
	/boot/vmlinuz-4.33.0-3-amd64
	/boot/initrd.img-4.33.0-3-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS remove 4.22.0-2-amd64 /boot/vmlinuz-4.22.0-2-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.33.0-3-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.11.0-1-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks remove (1 left)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/vmlinuz-4.22.0-2-amd64
	/boot/initrd.img-4.22.0-2-amd64
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS remove 4.11.0-1-amd64 /boot/vmlinuz-4.11.0-1-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	assert_equal "$status" "0"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.22.0-2-amd64 /vmlinuz.[0-9]"
	assert_contains_line "$output" "^ln -s /boot/vmlinuz-4.22.0-2-amd64 /vmlinuz.old.[0-9]"
}

@test "linux-update-symlinks remove (no images left)" {
	export IMAGE_LIST_FILES_IN_BOOT=$(cat <<-EOF
	/boot/.
	/boot/..
	EOF
	)

	run $LINUX_UPDATE_SYMLINKS remove 4.11.0-1-amd64 /boot/vmlinuz-4.11.0-1-amd64
        unset IMAGE_LIST_FILES_IN_BOOT

	echo "$output"
	assert_equal "$status" "0"
	assert_contains_line "$output" "^unlink /vmlinuz$"
	assert_contains_line "$output" "^unlink /vmlinuz.old$"
}
