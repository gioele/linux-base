#!/usr/bin/env bats

load ../lib/debian-linux.sh

## linux-version compare

LINUX_VERSION="$BATS_TEST_DIRNAME/../bin/linux-version"

@test "linux-version compare lt (true)" {
	run $LINUX_VERSION compare "2" "lt" "3"
	[ "$status" -eq 0 ]
}

@test "linux-version compare lt (false)" {
	run $LINUX_VERSION compare "3" "lt" "2"
	[ "$status" -eq 1 ]
}

@test "linux-version compare le (true)" {
	run $LINUX_VERSION compare "2" "le" "3"
	[ "$status" -eq 0 ]
}

@test "linux-version compare le (false)" {
	run $LINUX_VERSION compare "3" "le" "2"
	[ "$status" -eq 1 ]
}

@test "linux-version compare eq (true)" {
	run $LINUX_VERSION compare "5" "eq" "5"
	[ "$status" -eq 0 ]
}

@test "linux-version compare eq (false)" {
	run $LINUX_VERSION compare "5" "eq" "4"
	[ "$status" -eq 1 ]
}

@test "linux-version compare ge (true)" {
	run $LINUX_VERSION compare "2.1" "ge" "2.1"
	[ "$status" -eq 0 ]
}

@test "linux-version compare ge (false)" {
	run $LINUX_VERSION compare "2.4" "ge" "2.7"
	[ "$status" -eq 1 ]
}

@test "linux-version compare gt (true)" {
	run $LINUX_VERSION compare "5" "gt" "4"
	[ "$status" -eq 0 ]
}

@test "linux-version compare gt (false)" {
	run $LINUX_VERSION compare "5" "gt" "5"
	[ "$status" -eq 1 ]
}
